
import Header from './components/Header';
import Footer from './components/Footer';
import { Case } from './components/Case';
import { Item, itemLoader } from './components/Item';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
const router = createBrowserRouter([
  {
    path: "/",
    element: <Case/>,
  
  },
  {
    path: "/item/:id",
    element: <Item/>,
    
        loader: itemLoader, 
  }
  
]);

function App() {
  return (
    <div className="wrapper">
    <Header/>
    <RouterProvider router={router} /> 
    <Footer/>
    </div>
  );
}

export default App;
