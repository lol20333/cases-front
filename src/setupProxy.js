const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    ['/qwe', '/case'],
    createProxyMiddleware({
      target: 'https://localhost:8443',
      changeOrigin: true,
	  logLevel: 'debug',
      secure: false
    })
  );
};
