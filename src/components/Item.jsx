import React, { useEffect, useState } from "react";

import { Card, Typography } from "@mui/material";
import { getItem } from "../api/api";
import { Outlet, useLoaderData } from "react-router-dom";
import styles from '../styles/Item.css';


export async function itemLoader({ params }) {
    const item = await getItem(params.id);
    return { item };
  }

export const Item = (props) => {
    const {item} = useLoaderData();
    

    return (
        <div style={styles}>
            <a className="Btn">2$</a>
            <h1>Топовые айтемы</h1>
            <div style={{ display:"flex", flexWrap:"wrap"} }>
                {item ? item.map(c =>
                    <div style={{ "&:hover": {
                        backgroundColor: "#ccc",
                    },border: "1px solid #ccc",backgroundColor: "#111", width: "20%", color: '#fff' }} >
                        <Typography>
                            {c.skinId.name}
                        </Typography>
                        <Typography>
                            {c.skinId.type}
                        </Typography>
                        <img key={c.skinId.id} src={c.skinId.img} />
                    </div>) : "LOL"}
                    <Outlet/>
            </div>
        </div>
    )
}