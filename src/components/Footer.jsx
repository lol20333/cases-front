import React from 'react'
import styles from '../styles/Footer.css';

export default function Footer() {
  return (
    <footer style={styles}>
        Все права защищены &copy;
    </footer>
  )
}
