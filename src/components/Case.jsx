import React, { useEffect, useState } from "react";
import { getCase } from "../api/api";
import { Card, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import styles from '../styles/Case.css';



export const Case = (props) => {
    const [data, setData] = useState(null);

    useEffect(() => {
        getCase()
            .then(response => {
                setData(response)
            })
    }, [])

    return (
        <div style={styles}> 
            <h1>Топовые кейсы</h1>
            <div className="CaseBox" >
                {data ? data.map(c =>
                    <Link to={`/item/${c.id}`}>
                        <div style={{ 
                            backgroundColor: "#111",
                            color: '#fff',
                            border: "1px solid #ccc", width: "100%"
                        }} >
                            <Typography>
                                {c.name}
                            </Typography>
                            <Typography>
                                {c.key_coast}
                            </Typography>
                            <img key={c.id} src={c.img} />
                        </div>
                    </Link>
                ) : "LOL"}
            </div>
        </div>
    )
}