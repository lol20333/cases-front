export const getCase = () => {
    return fetch(`/case/container-list`, {
        method: "GET",
    })
    .then(response => response.json())
}
export const getItem = (case_id) => {
    return fetch(`/case/items-list?case_id=${case_id}`, {
        method: "GET",
    })
    .then(response => response.json())
}
export const getAuth = () => {
    return fetch(`/auth`, {
        method: "GET",
    })
    .then(response => response.json())
}